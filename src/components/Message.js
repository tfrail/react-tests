import React, { Component } from 'react';
import {Button, Segment, Icon} from 'semantic-ui-react';


class Message extends Component {
  counter = 0;
  state = {
    introText: this.props.msg1
  }
  
  handleClick = () => {
    this.counter++;
    if (this.counter % 2 === 0) {
      this.setState({introText:this.props.msg1});
    } else {
      this.setState({introText:this.props.msg2});
    }
  }

  render() {
    console.log(this.props)
    return (
      <Segment>
      <p>{this.state.introText}</p>
        <Button color="red" onClick={this.handleClick}><Icon name="thumbs up" /> {this.props.btnText} </Button>
        <img src={this.props.imgSrc} alt={this.props.imgAlt} />
        <p>This is more test text with a link. Click <a href={this.props.linkPath}>{this.props.linkText}</a> to go to new page</p>
      </Segment>
    );
  }
}

export default Message;
