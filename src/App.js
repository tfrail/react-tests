import React, { Component } from 'react';
import Message from './components/Message';

class App extends Component {

  render() {
    const props = {
      msg1: "This is a test",
      msg2: "This is a new test",
      btnText:"Test React Button",
      imgAlt: "test img testing",
      imgSrc: "https://upload.wikimedia.org/wikipedia/commons/f/f4/Florida_Box_Turtle_Digon3_re-edited.jpg",
      linkText: "this text",
      linkPath: "http://www.google.com"
    }
    return (
      <div className="App">
        <h2>React Just Testing Stuff</h2>
        <Message {...props} />
      </div>
    );
  }
}

export default App;
