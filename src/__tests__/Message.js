import React from 'react'; 
import { render, fireEvent } from 'react-testing-library'
import Message from '../components/Message';


describe('Message', () => { 
  it('should contain a button with text, and an intro paragraph', () => {

    const props = {
      msg1: "This is a test",
      msg2: "This is a new test",
      btnText:"Test React Button",
      imgAlt: "test img testing",
      imgSrc: "https://upload.wikimedia.org/wikipedia/commons/f/f4/Florida_Box_Turtle_Digon3_re-edited.jpg",
      linkText: "this text",
      linkPath: "http://www.google.com"
    }
    const {getByText, container, getByAltText} = render(<Message {...props} />); // getByText will search all elements that have a text node with textContent matching the given TextMatch
    const testBtn = getByText("Test React Button");

    expect(getByText("Test React Button")).toBeInTheDocument();
    expect(getByText("This is a test")).toBeInTheDocument();
    fireEvent.click(testBtn)  // Clicks the button, which should change the text
    expect(getByText("This is a new test")).toBeInTheDocument(); // Checks for new text after button click
    
    expect(getByAltText(/^test img*/)).toBeInTheDocument(); // Finds an image with alt text matching regex pattern
    expect(getByText((text, el) => { // Finds an element with a src = props.imgSrc --- Just to test an image source
      return el.src === "https://upload.wikimedia.org/wikipedia/commons/f/f4/Florida_Box_Turtle_Digon3_re-edited.jpg";
    }));
    expect(getByText("this text")).toBeInTheDOM;
    expect(getByText((text, el) => {  // Finds an element that is an "a" tag, and tests the href property of it
      return el.tagName.toLowerCase() === "a" && el.getAttribute("href") === "http://www.google.com";
    }));

   //expect(container.firstChild).toMatchSnapshot()  // Checks that the first child was not corrupted 
  })
})